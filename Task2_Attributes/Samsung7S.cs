﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task2_Attributes
{
    [Info("Italy", "2009.12.12", AppName = "Samsung7SApp", Version = 7.01F, Release = "2009.12.25")]
    class Samsung7S
    {
        private float price;

        public Samsung7S(float price)
        {
            this.price = price;
        }

        [Info("Italy", "2008.12.12")]
        public void CountSteps()
        {
            Random stepsAmount = new Random(1000);
            Console.WriteLine("step: {0}", stepsAmount);
        }
    }
}