﻿ using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task2_Attributes
{
    [Info("USA", "2005.08.10", AppName = "Samsung5SApp", Version = 5.01F, Release = "2005.08.02")]
    public class Samsung5S
    {
        private string type;

        public Samsung5S(string type)
        {
            this.type = type;
        }

        [Info("USA", "2004.10.15")]
        public void CallTheHelp()
        {
            Console.WriteLine("Calling.");
        }
    }
}
