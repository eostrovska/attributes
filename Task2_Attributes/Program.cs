﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Task2_Attributes;

namespace Task2_Attributes
{
    class Program
    {
        public static void ShowAttributeInfo(object obj)
        {
            Type typeOfApp = obj.GetType();

            object[] attributes = typeOfApp.GetCustomAttributes(true);

            foreach (InfoAttribute attribute in attributes)
            {
                Console.WriteLine("{0}, {1}, {2}, {3}, {4}", attribute.AppName, attribute.Release, attribute.DateOfManufacture, attribute.ManufacturerCountry, attribute.Version);
                attribute.SomeMethod();
            }
        }

        public static void ShowListOfClassesWithReleaseAndVersion(object[] listOfClasses, float version, string release)
        {
            foreach (var element in listOfClasses)
            {
                Type type = element.GetType();
                object[] attributes = type.GetCustomAttributes(false);

                foreach (InfoAttribute attribute in attributes)
                {
                    if (attribute.Version == version || attribute.Release == release)
                    {
                        Console.WriteLine(attribute.AppName);
                    }
                }
            }
        }

        static void Main(string[] args)
        {
            Console.WriteLine("Task 2: Create an attribute that can only be used for classes and methods." +
                              "When using it, you must pass two positional and three named parameters (for example, version and release date)" +
                              "Next, using the reflection mechanism, display metadata for these attributes and also display the list of classes version = 7.01" +
                              "or have a release date from the beginning of November 2017.");
            Console.WriteLine(new string('-', 60));

            Samsung5S app5S = new Samsung5S("Vip");
            Samsung6S app6S = new Samsung6S(2000);
            Samsung7S app7S = new Samsung7S(3500F);
            Object[] listOfClasses = new Object[] { app5S, app6S, app7S };

            ShowAttributeInfo(app5S);
            ShowAttributeInfo(app6S);
            ShowAttributeInfo(app7S);
            ShowListOfClassesWithReleaseAndVersion(listOfClasses, 7.01F, "2017.11.02");

            Console.ReadKey();
        }
    }
}
