﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task2_Attributes
{
    [Info("France", "2007.10.15", AppName = "Samsung6SApp", Version = 6.01F, Release = "2017.11.02")]
    class Samsung6S
    {
        private int memory;

        public Samsung6S(int memory)
        {
            this.memory = memory;
        }

        [Info("France", "1990.10.15")]
        public void Call()
        {
            Console.WriteLine("Calling.");
        }
    }
}
