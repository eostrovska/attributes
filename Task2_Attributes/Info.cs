﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task2_Attributes
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = false)]
    class InfoAttribute : System.Attribute
    {
        private string manufacturerCountry;
        private string dateOfManufacture;
        private float version;
        private string release;
        private string appName;

        public string ManufacturerCountry
        {
            set { manufacturerCountry = value; }
            get { return manufacturerCountry; }
        }

        public string DateOfManufacture
        {
            set { dateOfManufacture = value; }
            get { return dateOfManufacture; }
        }

        public float Version
        {
            set { version = value; }
            get { return version; }
        }

        public string Release
        {
            set
            {
                release = value;
                Convert.ToDateTime(release);
            }
            get { return release; }
        }

        public string AppName
        {
            set { appName = value; }
            get { return appName; }
        }

        public InfoAttribute(string manufacturerCountry, string dateOfManufacture)
        {
            this.manufacturerCountry = manufacturerCountry;
            this.dateOfManufacture = dateOfManufacture;
            Convert.ToDateTime(dateOfManufacture);
        }

        public void SomeMethod()
        {
            Console.WriteLine("Some Method.");
        }
    }
}
