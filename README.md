### What is this repository for? ###

* QTask 2: Create an attribute that can only be used for classes and methods.
  When using it, you must pass two positional and three named parameters (for example, version and release date)
  Next, using the reflection mechanism, display metadata for these attributes and also display the list of classes version = 7.01
  or have a release date from the beginning of November 2017.